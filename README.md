### Type conversions with String.js

This first introduction will cover the following functions:

```javascript
    S('123').toInt()
    S('123.456').toFloat()
    S({say: 'Hello world'}).toCSV()
```

For more information, please refer to the documentation: http://stringjs.com.