var S = require('string');

module.exports = function conversions(foo) {
    foo.toInt = S('12').toInt();
    console.log('To integer', typeof foo.toInt);

    foo.toFloat = S('12.12').toFloat();
    foo.toFloat = S('12.125125').toFloat(2);
    console.log('To float', typeof foo.toFloat, foo.toFloat);

    foo.keys = S({author: 'Coder Power', title: 'Conversions'}).toCSV({keys: true}).s;
    console.log('Object keys to CSV', foo.keys);

    foo.values = S({author: 'Coder Power', title: 'Conversions'}).toCSV().s;
    console.log('Object values to CSV', foo.values);

    return foo;
};
