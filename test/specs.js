var S = require('string');
var chai = require('chai');
var expect = chai.expect;

// Sources
var conversions = require('../sources/conversions');
var foo = conversions({});

describe('Type conversions', function() {
    it('should return an object', function(done) {
        expect(foo).to.be.an('object');
        done();
    });
    it('the object should have the keys: toInt, toFloat, keys, values', function(done) {
        expect(foo.toInt).to.exist;
        expect(foo.toFloat).to.exist;
        expect(foo.keys).to.exist;
        expect(foo.values).to.exist;
        done();
    });
    it('The key toInt should be an integer', function(done) {
        expect(foo.toInt).to.be.a('number');
        expect(S(foo.toInt).contains('.')).to.be.false;
        done()
    });
    it('The key toFloat should be a float', function(done) {
        expect(foo.toFloat).to.be.a('number');
        expect(S(foo.toFloat).contains('.')).to.be.true;
        done();
    });
});
